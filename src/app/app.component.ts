import { Component, ViewChild } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material/sidenav';

import { AuthService } from './services/auth.service';
import { MenuService } from './services/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(MatSidenav) private sidenav: MatSidenav;
  opened: boolean
  mode: string

  constructor(private media: ObservableMedia, 
    public authService: AuthService,
    private menuService: MenuService) {}

  ngOnInit() {
    this.media.subscribe((change: MediaChange) => {
      let screenSize = change.mqAlias
      if(screenSize == 'xs' || screenSize == 'sm') {
        this.opened = false
        this.mode = "over"
      } else {
        this.opened = true
        this.mode = "side"
      }
    })
    this.menuService.menutToggled$.subscribe((data: boolean) => {
      if(data) {
        this.sidenav.toggle()
      }
    })
  }

  closeSideNav() {
    if(this.media.isActive('xs') || this.media.isActive("sm"))
      this.sidenav.close()
  }
}
