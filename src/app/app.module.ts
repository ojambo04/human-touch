import { CourseService } from './services/course.service';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { environment } from '../environments/environment.prod';
import { AppMaterialModule } from './app-material/app-material.module';
import { AppRouterModule } from './app-router/app-router.module';
import { AppComponent } from './app.component';
import { StudentFormComponent } from './components/student-form/student-form.component';
import { StudentComponent } from './components/student/student.component';
import { UploadCetificateComponent } from './components/upload-cetificate/upload-cetificate.component';
import { FooterComponent } from './core/footer/footer.component';
import { ForgetPasswordComponent } from './core/forget-password/forget-password.component';
import { HeaderComponent } from './core/header/header.component';
import { LoginComponent } from './core/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { LoggedinGuardService } from './services/loggedin-guard.service';
import { MenuService } from './services/menu.service';
import { StudentService } from './services/student.service';
import { CourseFormComponent } from './components/course-form/course-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    StudentComponent,
    UploadCetificateComponent,
    LoginComponent,
    ForgetPasswordComponent,
    StudentFormComponent,
    CourseFormComponent
  ],
  entryComponents: [
    StudentFormComponent,
    CourseFormComponent
  ],
  imports: [
    BrowserModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    AppRouterModule,
    FlexLayoutModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    ReactiveFormsModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    FormsModule
  ],
  providers: [
    MenuService,
    AuthGuardService,
    AuthService,
    LoggedinGuardService,
    StudentService,
    MatDatepicker,
    CourseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
