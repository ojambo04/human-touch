import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { Course } from '../shared/model/course';
import { CourseId } from './../shared/model/course';

@Injectable()
export class CourseService {
  url = "courses"

  constructor(private storage: AngularFireStorage, private db: AngularFirestore) { }

  searchCourses(aadhar: string): Observable<CourseId[]> {
    return this.db.collection(this.url, ref => ref.where("aadhar", "==", aadhar))
      .snapshotChanges().map((actions: DocumentChangeAction[]) => {
        return actions.map((action: DocumentChangeAction) => {
          const key = action.payload.doc.id
          const data = action.payload.doc.data() as Course
          data.assessment_date = new Date(data.assessment_date.seconds * 1000)
          data.certificate_issue_date = new Date(data.certificate_issue_date.seconds * 1000)
          return { id: key, ...data }
        })
      })
  }

  create(course: Course) {
    return this.db.collection(this.url).add(course).catch(this.handleError)
  }

  update(id: string, course: Course) {
    return this.db.collection(this.url).doc(id).update(course).catch(this.handleError)
  }

  deleteCourse(course: CourseId) {
    return this.db.collection(this.url).doc(course.id).delete()
    .then(() => {
      console.log("deleted")
      return this.storage.ref(course.storageUrl).delete().toPromise()
    })
  }

  uploadFile(path: string, file) {
    return this.storage.ref(`${this.url}/${path}`).put(file)
  }

  updateFileUrl(courseId: string, storageUrl: string) {
    return this.storage.ref(storageUrl).getDownloadURL().toPromise().then(url => {
      this.db.doc(`${this.url}/${courseId}`).update({fileUrl: url, storageUrl: storageUrl})
    })
  }

  handleError(error) {
    console.log("Course Service Error", error)
  }
}
