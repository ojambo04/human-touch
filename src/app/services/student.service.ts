import { Course, CourseId } from './../shared/model/course';
import { QueryDocumentSnapshot } from '@firebase/firestore-types';
import { StudentId, Student } from './../shared/model/student';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { User } from '@firebase/auth-types';
import { CourseService } from './course.service';

@Injectable()
export class StudentService {

  constructor(private db: AngularFirestore, private courseService: CourseService) { 
    const settings = {timestampsInSnapshots: true};
    db.app.firestore().settings(settings);
  }

  getTotalLength(): Observable<number> {
    return this.db.collection("students").valueChanges().map(data => data.length)
  }

  addUser(student: Student) {
    return this.db.collection("students").doc(student.aadhar).set(student).catch(this.handleError)
  }

  searchStudent(aadhar: string) {
    return this.db.collection("students", ref => ref.where("aadhar", "==", aadhar).limit(1)).valueChanges().take(1)
  }

  getStudents(): Observable<StudentId[]> {
    return this.db.collection("students").snapshotChanges().map((actions: DocumentChangeAction[]) => {
      return actions.map((action: DocumentChangeAction) => {
        const key = action.payload.doc.id
        const data = action.payload.doc.data() as Student
        data.dob = new Date(data.dob.seconds * 1000)
        return { id: key, ...data }
      })
    })
  }

  getList(limit: number): Observable<QueryDocumentSnapshot[]> {
    return this.db.collection("students", ref => ref.limit(limit))
      .snapshotChanges().map((actions: DocumentChangeAction[]) => {
        return actions.map((a: DocumentChangeAction) => {
          return a.payload.doc
        })
      })
  }
  
  getNextPageList(limit: number, lastDoc): Observable<QueryDocumentSnapshot[]> {
    return this.db.collection("students", ref => ref.startAfter(lastDoc).limit(limit))
      .snapshotChanges().map((actions: DocumentChangeAction[]) => {
        return actions.map((a: DocumentChangeAction) => {
          return a.payload.doc
        })
      })
  }

  updateStudent(id:string, student: Student) {
    return this.db.collection("students").doc(id).update(student)
  }

  deleteStudent(id: string, aadhar: string) {
    return this.db.collection("students").doc(id).delete().then(() => {
      this.courseService.searchCourses(aadhar)
      .take(1)
      .subscribe(courses => 
        courses.forEach(course => 
          this.courseService.deleteCourse(course))
      )
    })
  }

  private handleError(error) {
    console.log("Student Service Error", error)
  }
}
