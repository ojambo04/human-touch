import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthService {

  constructor(private afAuth: AngularFireAuth, 
    private route: ActivatedRoute,
    private router: Router) { }

  loginWithEmail(email: string, password: string) {
    let returnUrl = this.route.snapshot.queryParamMap.get("returnUrl") || "/"

    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.router.navigateByUrl(returnUrl)
      })
  }

  logout() {
    this.afAuth.auth.signOut()
    return this.router.navigate(['/admin/login'])
  }

  resetPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email)
  }

  get currentUser() {
    return this.afAuth.auth.currentUser
  }

  get currentUser$() {
    return this.afAuth.authState
  }
}
