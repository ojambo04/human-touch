import { TestBed, inject } from '@angular/core/testing';

import { StorageErrorHandler } from './storage-error-handler';

describe('StorageErrorHandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StorageErrorHandler]
    });
  });

  it('should be created', inject([StorageErrorHandler], (service: StorageErrorHandler) => {
    expect(service).toBeTruthy();
  }));
});
