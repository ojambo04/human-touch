import { Injectable } from '@angular/core';

@Injectable()
export class StorageErrorHandler {

  handleError(error) {
    switch(error['code']) {
      case 'storage/unauthorized':
        alert("You are not authorized to access file storage server")
        break;
      case 'storage/object-not-found':
        alert("This file does not exist on the server any more")
        break;
      case 'storage/canceled':
        alert("You have canceled the process")
        break;  
      case 'storage/unknown':
        alert("An unkown error occured")
        break;    
    }
  }
}

