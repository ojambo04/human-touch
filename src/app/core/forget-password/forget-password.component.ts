import { AuthenticationValidator } from './../../shared/validators/authentication.validator';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss', '../login/login.component.scss']
})
export class ForgetPasswordComponent {
  resetPassForm: FormGroup
  errorMsg: string
  reset = false;

  constructor(private fb: FormBuilder, private authService: AuthService) { 
    this.resetPassForm = this.fb.group({
      email: ['', [Validators.required, AuthenticationValidator.validateEmail]]
    })
  }

  resetPassword() {
    if(this.resetPassForm.invalid) return
    this.authService.resetPassword(this.email.value).then(() => {
      this.reset = true
    })
    .catch(error => {
      console.log(error)
      this.errorMsg = error.message
    })
  }

  get email() {
    return this.resetPassForm.get('email')
  }
}
