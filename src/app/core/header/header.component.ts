import { Component } from '@angular/core';

import { AuthService } from '../../services/auth.service';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(private menuService: MenuService, private authService: AuthService) {}

  toggleMenu() {
    this.menuService.menuToggle(true)
  }

  logout() {
    this.authService.logout()
  }
}
