import { AuthenticationValidator } from './../../shared/validators/authentication.validator';
import { AuthService } from './../../services/auth.service';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup
  errorMsg: string

  constructor(public authService: AuthService, private fb: FormBuilder) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, AuthenticationValidator.validateEmail]],
      password: ['', Validators.required]
    })
  }

  logout() {
    this.authService.logout()
  }

  loginWithEmail() {
    if(this.loginForm.invalid) return

    this.authService.loginWithEmail(this.email.value, this.password.value)
      .catch(error => this.handleError(error))
  }

  get email() {
    return this.loginForm.get('email')
  }

  get password() {
    return this.loginForm.get('password')
  }

  handleError(error) {
    console.log(error)
    switch(error.code) {
      case 'auth/user-not-found':
        this.errorMsg = "There is no user record for the provided email"
        break;
      case 'auth/wrong-password':
        this.errorMsg = "The password entered is invalid"
        break;   
    }
  }

}
