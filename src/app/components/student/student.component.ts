import { Component, OnDestroy, OnInit } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { MatDialog } from '@angular/material';
import { Timestamp, QueryDocumentSnapshot, DocumentSnapshot } from '@firebase/firestore-types';
import { Subscription } from 'rxjs/Subscription';

import { StudentService } from '../../services/student.service';
import { StudentId, Student } from '../../shared/model/student';
import { StudentFormComponent } from './../student-form/student-form.component';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit, OnDestroy {
  pageLength: number;
  pageSize = 10;
  students: StudentId[];
  displayedColumns = [ "name", "aadharNo", "gender", "dob", "phone", "email", "actions"];
  subscription: Subscription;
  pageIndexDocs: QueryDocumentSnapshot[] = [];

  constructor(private studentService: StudentService, 
    private matDialog: MatDialog,
    private media: ObservableMedia) { }

  ngOnInit() {
    this.subscription = this.studentService.getList(this.pageSize)
    .switchMap((queryDocSnap: DocumentSnapshot[]) => {
      this.students = this.formatData(queryDocSnap)
      this.pageIndexDocs[0] = queryDocSnap[queryDocSnap.length-1]

      return this.studentService.getTotalLength()
    })
    .subscribe(length => this.pageLength = length)

    this.media.subscribe((change: MediaChange) => {
      let screenSize = change.mqAlias
      if(screenSize == 'xs') {
        this.displayedColumns = [ "name", "aadharNo", "actions"]
      }
      else if(screenSize == 'sm') {
        this.displayedColumns = [ "name", "aadharNo", "gender", "phone", "actions"]
      }
      else if(screenSize == "md") {
        this.displayedColumns = [ "name", "aadharNo", "gender", "dob", "phone", "actions"]
      }
      else {
        this.displayedColumns = [ "name", "aadharNo", "gender", "dob", "phone", "email", "actions"]
      }
    })
  }

  addStudent() {
    this.matDialog.open(StudentFormComponent, {
      height: '450px',
      width: '550px'
    })
  }

  studentDetail(student: StudentId) {
    this.matDialog.open(StudentFormComponent, {
      height: '450px',
      width: '550px',
      data: student
    })
  }

  onDelete(student: StudentId) {
    let retVal = confirm("Are you sure you want to delete?")
    if(retVal == true) {
      this.studentService.deleteStudent(student.id, student.aadhar)
    }
  }

  changePage(pageEvent) {
    let observer;
    let pageSize = pageEvent['pageSize']
    let pageIndex = pageEvent['pageIndex']

    if(pageIndex == 0) 
      observer = this.studentService.getList(pageSize)
    else
      observer = this.studentService.getNextPageList(pageSize, this.pageIndexDocs[pageIndex])

    observer.take(1).subscribe((queryDocSnap: QueryDocumentSnapshot[]) => {
      this.students = this.formatData(queryDocSnap)
      this.pageIndexDocs[pageIndex+1] = queryDocSnap[queryDocSnap.length-1]
    })
  }

  private formatData(queryDocSnap: DocumentSnapshot[]) {
    return queryDocSnap.map(action => {
      let data = action.data() as Student
      data.dob = new Date(data.dob.seconds * 1000)  
      let id = action.id
      return { id, ...data }
    })
  }

  private formatDate(date: Date) {
    let month = (date.getMonth()+1).toString()
    month = parseInt(month) > 9 ? month : "0" + month
    
    return date.getDate() + '-' + month + '-' + date.getFullYear()
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }
}
