import { AuthenticationValidator } from './../../shared/validators/authentication.validator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { StudentService } from '../../services/student.service';
import { Student, StudentId } from '../../shared/model/student';
import { DocumentReference, Timestamp } from '@firebase/firestore-types';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.scss']
})
export class StudentFormComponent implements OnInit {
  studentForm: FormGroup
  genderTypes = ["Male", "Female", "Others"]

  constructor(private fb: FormBuilder, 
    public dialogRef: MatDialogRef<StudentFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: StudentId,
    private studentService: StudentService) { }

  ngOnInit() {
    let student = this.initializeStudentForm()
    this.createForm(student);
    
    if(this.data) this.aadhar.disable()
  }

  onSubmit() {
    if(this.studentForm.invalid) return

    let student = this.studentForm.value as Student
    if(this.data)
      this.studentService.updateStudent(this.data.id, student)
    else
      this.studentService.addUser(student).then((docRef) => this.studentForm.reset())
    
    this.dialogRef.close()  
  }

  // onDelete() {
  //   if(!this.data) return
    
  //   let retVal = confirm("Are you sure you want to delete?")
  //   if(retVal == true) {
  //     this.studentService.deleteStudent(this.data.id)
  //     this.dialogRef.close()
  //   }
  // }

  initializeStudentForm(): Student {
    let student: Student = {} as Student
    if(this.data) {
      student = this.data
      console.log(student.dob)
    }
    else {
      student.name = ""
      student.aadhar = ""
      student.dob = ""
      student.email = ""
      student.gender =""
      student.phone = ""
      student.address = ""
    }
    return student
  }

  createForm(student: Student) {
    this.studentForm = this.fb.group({
      name: [student.name, Validators.required],
      aadhar: [student.aadhar, [Validators.required, Validators.pattern('[0-9]{12}')]],
      gender: [student.gender, Validators.required],
      dob: [student.dob, Validators.required],
      phone: [student.phone, [Validators.required, Validators.pattern('[1-9][0-9]{9}')]],
      email: [student.email, [AuthenticationValidator.validateEmail]],
      address: [student.address, Validators.required]
    })
  }

  get name() {
    return this.studentForm.get("name")
  }

  get aadhar() {
    return this.studentForm.get("aadhar")
  }

  get gender() {
    return this.studentForm.get("gender")
  }

  get dob() {
    return this.studentForm.get("dob")
  }

  get phone() {
    return this.studentForm.get("phone")
  }

  get email() {
    return this.studentForm.get("email")
  }

  get address() {
    return this.studentForm.get("address")
  }
}
