import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageErrorHandler } from './../../services/storage-error-handler';
import { Course, CourseId } from './../../shared/model/course';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { AngularFireUploadTask } from 'angularfire2/storage';
import { UploadTaskSnapshot } from '@firebase/storage-types';
import { CourseService } from '../../services/course.service';
import { DocumentReference } from '@firebase/firestore-types';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss'],
  providers: [ StorageErrorHandler ]
})
export class CourseFormComponent implements OnInit {
  courseForm: FormGroup;
  selectedFile;
  courseId;
  completed = false;
  submitted = false;
  progressValue = 0;
  title;


  constructor(public dialogRef: MatDialogRef<CourseFormComponent>, 
    private stErrorHandler: StorageErrorHandler,
    private courseService: CourseService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {
    let course = this.initializeForm()
    this.createForm(course)
  }
    
  saveCourse() {
    this.submitted = true;
    let course = this.courseForm.value
    course.aadhar = this.data.aadhar

    if(this.data.course) {
      this.courseService.update(this.data.course.id, course).then(() => { 
        this.completed = true 
        this.submitted = false
      })
    }
    else {
      this.courseService.create(course).then((docRef: DocumentReference) => {
        if(!docRef) return 
        this.courseId = docRef.id
        this.completed = true 
        this.submitted = false
      }) 
    }  
  }  

  deleteCourse() {
    
  }

  selectFile(event) {
    this.selectedFile = event.target.files[0]
  }

  uploadFile() {
    if(!this.selectedFile || !this.courseId) return;
    
    let uploadTask: AngularFireUploadTask = this.courseService.uploadFile(this.courseId, this.selectedFile)
    uploadTask.percentageChanges().subscribe(val => this.progressValue = val, error => {})
    uploadTask.then((snapshot: UploadTaskSnapshot)=> {
      this.courseService.updateFileUrl(this.courseId, snapshot.metadata.fullPath)
      setTimeout(()=>{
        this.dialogRef.close()
      }, 400)
    }, error => {
      this.dialogRef.close()
      this.stErrorHandler.handleError(error)
    })
  }

  initializeForm() {
    let course = {} as Course

    if(this.data.course) {
      course = this.data.course
      this.courseId = this.data.course.id
      this.title = "Update Existing Course"
    }
    else {
      course.name = ""
      course.assessment_date = null
      course.certificate_issue_date = null
      course.grade_obtained = ""
      course.training_agency = ""
      course.training_center = ""
      this.title = "Add New Course"
    }
    return course
  }

  createForm(course: Course) {
    this.courseForm = this.fb.group({
      name: [course.name, Validators.required],
      grade_obtained: [course.grade_obtained, Validators.required],
      assessment_date: [course.assessment_date, Validators.required],
      training_agency: [course.training_agency, Validators.required],
      training_center: [course.training_center, Validators.required],
      certificate_issue_date: [course.certificate_issue_date, Validators.required]
    })
  }

}
