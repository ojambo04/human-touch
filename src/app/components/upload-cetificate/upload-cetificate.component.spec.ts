import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadCetificateComponent } from './upload-cetificate.component';

describe('UploadCetificateComponent', () => {
  let component: UploadCetificateComponent;
  let fixture: ComponentFixture<UploadCetificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadCetificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadCetificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
