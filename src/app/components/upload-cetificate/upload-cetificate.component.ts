import { Component, OnInit } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

import { StudentService } from '../../services/student.service';
import { Course } from '../../shared/model/course';
import { CourseService } from './../../services/course.service';
import { CourseId } from './../../shared/model/course';
import { Student } from './../../shared/model/student';
import { CourseFormComponent } from './../course-form/course-form.component';

@Component({
  selector: 'app-upload-cetificate',
  templateUrl: './upload-cetificate.component.html',
  styleUrls: ['./upload-cetificate.component.scss']
})
export class UploadCetificateComponent implements OnInit {
  aadhar: string;
  student: Student;
  isSearching = false;
  message: string;
  subscription: Subscription;
  courses: CourseId[] = [];
  displayedColumns = [ "name", "grade_obtained", "assessment_date", "training_agency", "training_center", "certificate_url", "actions"];

  constructor(private studentService: StudentService, 
    private courseService: CourseService,
    private media: ObservableMedia,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.media.subscribe((change: MediaChange) => {
      let screenSize = change.mqAlias
      if(screenSize == 'xs') {
        this.displayedColumns = [ "name", "certificate_url", "actions"];
      }
      else if(screenSize == 'sm') {
        this.displayedColumns = [ "name", "grade_obtained", "training_agency", "certificate_url", "actions"];
      }
      else if(screenSize == "md") {
        this.displayedColumns = [ "name", "grade_obtained", "training_agency", "training_center", "certificate_url", "actions"];
      }
      else {
        this.displayedColumns = [ "name", "grade_obtained", "assessment_date", "training_agency", "training_center", "certificate_url", "actions"];
      }
    })
  }  

  onSearch() {
    if(!this.aadhar) return
    
    /* Reset before querying with new parameters */
    this.student = null
    this.courses = []
    if(this.subscription) this.subscription.unsubscribe()
    this.isSearching = true

    /* Retreive student record based on aadhar no */
    this.studentService.searchStudent(this.aadhar).subscribe(students => {
      let data = students[0] as Student   
      if(data) {
        this.student = data
        this.subscription = this.courseService.searchCourses(data.aadhar)
                              .subscribe(courses => this.courses = courses)
        this.message = ""                      
      }
      else {
        this.message = "Cannot find student record with that Aadhar No. Please try again!!"
      }

      this.isSearching = false
    })
  }

  addCourse() {
    if(!this.aadhar) return 

    this.dialog.open(CourseFormComponent, {
      width: "550px",
      height: "550px",
      data: { aadhar: this.aadhar}
    })
  }

  deleteCourse(course: CourseId) {
    let retVal = confirm("Are you sure you want to delete?")
    if(retVal == true) {
      this.courseService.deleteCourse(course)
    }
  }

  courseDetail(course: Course) {
    if(!this.aadhar) return 

    this.dialog.open(CourseFormComponent, {
      width: "550px",
      height: "550px",
      data: { aadhar: this.aadhar, course: course }
    })
  }

  formatDate(date: Date) {
    let month = (date.getMonth()+1).toString()
    month = parseInt(month) > 9 ? month : "0" + month
    
    return date.getDate() + '-' + month + '-' + date.getFullYear()
  }

  uploadStatus(course: Course) {
    if(course.fileUrl) return "Yes"
    else return "No"
  }

  ngOnDestroy() {
    if(this.subscription) this.subscription.unsubscribe()
  }
}
