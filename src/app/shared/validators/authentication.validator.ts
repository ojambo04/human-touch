import { FormControl, Validators } from "@angular/forms";

export class AuthenticationValidator {
	static validateEmail(c: FormControl): Validators | null {
		let str = c.value as string
		let regex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
		
		if(regex.test(str) || !str) 
			return null
		else
			return { invalidEmail: true }
	}
}