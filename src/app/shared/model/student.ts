import { Timestamp } from '@firebase/firestore-types';
export interface Student {
    name: string;
    gender: string;
    aadhar: string;
    dob: any;
    phone: string;
    email: string;
    address: string;
}

export interface StudentId extends Student {
    id: string;
}