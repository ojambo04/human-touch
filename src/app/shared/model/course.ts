import { Student } from './student';
export interface Course {
    aadhar: string;
    name: string;
    grade_obtained: string;
    assessment_date: any;
    training_agency: string;
    training_center: string;
    certificate_issue_date: any;
    fileUrl: string;
    storageUrl: string;
}

export interface CourseId extends Course {
    id: string;
}