import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { StudentComponent } from '../components/student/student.component';
import { UploadCetificateComponent } from '../components/upload-cetificate/upload-cetificate.component';
import { LoggedinGuardService } from '../services/loggedin-guard.service';
import { ForgetPasswordComponent } from './../core/forget-password/forget-password.component';
import { LoginComponent } from './../core/login/login.component';
import { AuthGuardService } from './../services/auth-guard.service';

const routes: Route[] = [
  { 
    path: "admin/student", 
    component: StudentComponent,
    canActivate: [ AuthGuardService ]
  },
  { 
    path: "admin/upload", 
    component: UploadCetificateComponent,
    canActivate: [ AuthGuardService ]
  },
  { 
    path: "admin/login", 
    component: LoginComponent ,
    canActivate: [ LoggedinGuardService ]
  },
  { 
    path: "admin/forget-password", 
    component: ForgetPasswordComponent,
    canActivate: [ LoggedinGuardService ] 
  },
  { 
    path: "", redirectTo: "/admin/upload", pathMatch: "full" 
  },
  { 
    path: "**", redirectTo: "/admin/upload", pathMatch: "full" 
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule { }
